﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using teamFourProjectTwo.Models;

namespace teamFourProjectTwo.Controllers.APIControllers
{
    public class SalesController : ApiController
    {
        private teamFourProjectTwoContext dbc = new teamFourProjectTwoContext();

        // GET: api/Sales
        public IQueryable<Sale> GetSalesByDB()
        {
            return dbc.Sales;
        }

        // GET: api/Sales/5
        [ResponseType(typeof(Sale))]
        public async Task<IHttpActionResult> GetSalesId(int id)
        {
            Sale money = await dbc.Sales.FindAsync(id);
            if (money == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return Ok(money);
        }

        // POST: api/Sales
        [ResponseType(typeof(Sale))]
        public async Task<IHttpActionResult> PostSale(Sale money)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            money.Date = DateTime.UtcNow;
            dbc.Sales.Add(money);
            await dbc.SaveChangesAsync();

            Sale money1 = await dbc.Sales.FindAsync(money.ID);

            return CreatedAtRoute("DefaultApi", new { id = money.ID }, money1);
        }

        // PUT: api/Sales/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSale(int id, Sale money)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != money.ID)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            dbc.Entry(money).State = EntityState.Modified;

            try
            {
                await dbc.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!SaleExists(id))
                {
                    return StatusCode(HttpStatusCode.NotFound);
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Sales/5
        [ResponseType(typeof(Sale))]
        public async Task<IHttpActionResult> DeleteSale(int id)
        {
            Sale money = await dbc.Sales.FindAsync(id);
            if (money != null)
            {
                dbc.Sales.Remove(money);
                await dbc.SaveChangesAsync();
            }
            else
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return Ok(money);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbc.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SaleExists(int id)
        {
            return dbc.Sales.Count(p => p.ID == id) > 0;
        }
    }
}
