using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TeamFourStar {
    public class CategoryFrag : EditorFrag {
        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            return inflater.Inflate(Resource.Layout.CatFrag , container, false);
        }
        public override void Populate(string[] CategoryData) {
            EditText IDText = View.FindViewById<EditText>(Resource.Id.IDEdit);
            EditText NameText = View.FindViewById<EditText>(Resource.Id.NameEdit);

            IDText.SetText(CategoryData[0],TextView.BufferType.Editable);


        }
        public override string[] GetResults() {
            string ID = View.FindViewById<EditText>(Resource.Id.IDEdit).Text;
            string Name = View.FindViewById<EditText>(Resource.Id.NameEdit).Text;

            string[] s = { ID , Name };
            return s;
        }
    }
}