﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using teamFourProjectTwo.Models;

namespace teamFourProjectTwo.Services
{
    public class PasswordHasher
    {
        teamFourProjectTwoContext db = new teamFourProjectTwoContext();

        public void SavePassword(string unhashedPassword)
        {
            string hashPassword = Crypto.HashPassword(unhashedPassword);
        }

        public bool CheckPassword(string unhashedPassword, User user)
        {
            return user != null && Crypto.VerifyHashedPassword(user.Password, unhashedPassword);
        }
    }
}