﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace teamFourProjectTwo.Models
{
    public class teamFourProjectTwoContext : DbContext
    { 
        public teamFourProjectTwoContext() : base("name=teamFourProjectTwoContext")
        {
            Database.SetInitializer<teamFourProjectTwoContext>(new teamFourProjectTwoDBInitializer());
        }

        public System.Data.Entity.DbSet<teamFourProjectTwo.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<teamFourProjectTwo.Models.Sale> Sales { get; set; }

        public System.Data.Entity.DbSet<teamFourProjectTwo.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<teamFourProjectTwo.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<teamFourProjectTwo.Models.Manufacturer> Manufacturers { get; set; }
 
    }
}
