﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using teamFourProjectTwo.ViewModels;

namespace teamFourProjectTwo.Models
{
    public class User
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [DisplayName("Email Address")]
        [DataType(DataType.EmailAddress)]
        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Column(TypeName = "varchar")]
        public string Password { get; set; }

        [Column(TypeName ="varchar")]
        public string ApiKey { get; set; }


        public UserViewModel ToViewModel()
        {
            return new UserViewModel()
            {
                Email = Email,
                Password = Password
            };
        }
    }
}