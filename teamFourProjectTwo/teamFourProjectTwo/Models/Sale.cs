﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace teamFourProjectTwo.Models
{
    public class Sale
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Purchase Date")]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        public List<Product> Products { get; set; }

        [DisplayName("Total Amount")]
        [DataType(DataType.Currency)]
        [Required]
        [Range(typeof(decimal), "0", "79228162514264337593543950335")]
        public decimal TotalAmount { get; set; }

        [Required]
        [DisplayName("Email Address")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(128)]
        public string EmailAddress { get; set; }

        public Sale()
        {
            this.Date = DateTime.UtcNow;
        }
        
    }
}