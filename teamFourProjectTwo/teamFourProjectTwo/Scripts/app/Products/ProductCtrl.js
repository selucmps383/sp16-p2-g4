﻿(function () {
    'use strict';

    angular
        .module('store')
        .controller('ProductCtrl', ProductCtrl);

    ProductCtrl.$inject = ['$scope', '$http', 'CategoryFactory', 'ManufacturerFactory', 'DataService'];

    function ProductCtrl($scope, $http, CategoryFactory, ManufacturerFactory, DataService) {
        var ProductCtrl = this;

        ProductCtrl.selectedCategories = CategoryFactory.selectedCategories;
        ProductCtrl.selectedManufacturers = ManufacturerFactory.selectedManufacturers;
        ProductCtrl.productsInCart = DataService.productsInCart;

        // Allows user to narrow search criteria to search through names, prices, categories, 
        // manufacturers, or all product properties
        ProductCtrl.searchOptions = [
            { label: 'Name', val: 'name'},
            { label: 'Price', val: 'price' },
            { label: 'Category', val: 'category' },
            { label: 'Manufacturer', val: 'manufacturer' }
        ];
        ProductCtrl.searchFilter = ProductCtrl.searchOptions[0];

        // Allows user to sort products by name, price, category, or manufacturer
        ProductCtrl.sortByOptions = [
            { label: 'Name', val: 'Name' }, 
            { label: 'Price', val: 'Price' }, 
            { label: 'Category', val: 'Category' },
            { label: 'Manufacturer', val: 'Manufacturer' }
        ];
        ProductCtrl.sortByValue = ProductCtrl.sortByOptions[0];

        // Allows user to sort products in ascending or descending order
        ProductCtrl.orderOptions = [
            { label: 'Ascending', val: false },
            { label: 'Descending', val: true }
        ];
        ProductCtrl.sortOrder = ProductCtrl.orderOptions[0];
        
        // Retrieve Products via API
        $http
            .get('http://localhost:59367/api/products')
            .then(function (response) {
                $scope.products = response.data;
            });

        // Adds product to shared cart within service
        $scope.addToCart = function (product, quantity) {
            DataService.addToCart(product, quantity);
        };

        // Clears search bar
        ProductCtrl.clearSearch = function () {
            if (Product.search.length > 0) {
                ProductCtrl.search = null;
            }
        };

        $scope.$watch(function () { return CategoryFactory.selectedCategories }, function (newVal, oldVal) {
            if (typeof newVal !== 'undefined') {
                ProductCtrl.selectedCategories = CategoryFactory.selectedCategories;
            }
        });

        $scope.$watch(function () { return ManufacturerFactory.selectedManufacturers }, function (newVal, oldVal) {
            if (typeof newVal !== 'undefined') {
                ProductCtrl.selectedManufacturers = ManufacturerFactory.selectedManufacturers;
            }
        });
        
        
        
    };
})();