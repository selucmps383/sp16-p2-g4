﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace TeamFourStar.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }

        [DisplayName("Created Date")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("Last Modified Date")]
        public DateTime LastModifiedDate { get; set; }

        public decimal Price { get; set; }

        [DisplayName("Inventory Count")]

        public int InventoryCount { get; set; }

        public virtual Category Category { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }
       
    }
}