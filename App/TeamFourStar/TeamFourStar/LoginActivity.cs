﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using TeamFourStar.Models;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable.Serializers;
using System.Threading.Tasks;

namespace TeamFourStar {
    [Activity(Label = "Login" , MainLauncher = true , Icon = "@drawable/icon")]
    public class LoginActivity : Activity {

        /*Carl's Old code
                public string uApiK = "";
                public string baseURL = "http://147.174.161.169:59367/";
                public int userID = -1;

                protected override void OnCreate(Bundle bundle) {
                    base.OnCreate(bundle);
                    // Set our view from the "main" layout resource
                    SetContentView(Resource.Layout.Login);

                   string username = FindViewById<EditText>(Resource.Id.UserNameTextBox).Text;
                    string Password = FindViewById<EditText>(Resource.Id.PasswordTextBox).Text;
                    Button button = FindViewById<Button>(Resource.Id.LoginButton);
                    button.Click +=  async (sender, e) => {

                        string url = "api/ApiKey?email=" + username + "&password=" + Password;
                        var fetch = await FetchApiKeyAndUser(url);
                        uApiK = fetch.ApiKey;
                        userID = fetch.ID;


                        if (username == fetch.Email && Password == fetch.Password)
                        {
                            EnterApp(username);
                        }
                        else
                        {
                            string toast = "Invalid Credintials, Please try again";
                            Toast.MakeText(this, toast, ToastLength.Long).Show();
                        }
                    };
                } */

        /*public async Task<User> FetchApiKeyAndUser(string url){
        RestClient client = new RestClient (baseURL);
        var request = new RestSharp.Portable.RestRequest (baseURL + url, Method.GET);
        var response = await client.Execute<User> (request);
        return response.Data;
        }*/

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Login);

            Button button = FindViewById<Button>(Resource.Id.LoginButton);
            button.Click += delegate { LogIn(); };
        }

        private void LogIn() {
            bool valid = validate();
            string username = FindViewById<EditText>(Resource.Id.UserNameTextBox).Text;
            string Password = FindViewById<EditText>(Resource.Id.PasswordTextBox).Text;

            if (valid) {
                EnterApp(username);
            } else {
                Toast.MakeText(this , "Invalid Credentials" , ToastLength.Long).Show();
            }
        }

        private bool validate() {
            return FindViewById<CheckBox>(Resource.Id.testcheckbox).Checked;
        }

        private void EnterApp(string username) {
            var ManagerIntent = new Intent(this,typeof(ManagerActivity));
			ManagerIntent.PutExtra("Message" , "Passed!");
            StartActivity(ManagerIntent);

        }
    }
}

