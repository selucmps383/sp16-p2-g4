﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Provider;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace TeamFourStar {
    public class StoreAPIListAdapter : BaseAdapter<string> {
        string[] names;
        Activity context;
        ManagerContext mc;

        public StoreAPIListAdapter(Activity context , string[] items, ManagerContext m) {
            names = items;
            this.context = context;
            mc = m;
        }

        public override string this[int position]
        {
            get
            { return names[position]; }
        }

        public override int Count
        {
            get { return names.Length; }
        }

        public override long GetItemId(int position) {
            return position;
        }

        public override View GetView(int position , View convertView , ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleExpandableListItem1 , null);
            }
            view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = names[position];
            return view;
        }
    }
}
