﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Security;
using teamFourProjectTwo.Models;
using teamFourProjectTwo.Services;

namespace teamFourProjectTwo.Controllers.API_Controllers
{
    public class ApiKeyController : ApiController
    {
        private teamFourProjectTwoContext dbc = new teamFourProjectTwoContext();

        // GET: api/ApiKey
        // Example: GET ApiKey?email=sa@383.com&password=password
        // 
        // This method will generate a new Api Key, and update the corresponding User's
        // ApiKey property with each successful request
        public async Task<IHttpActionResult> GetApiKey(string email, string password)
        {
            // Returns HTTP 400 when email or password is missing / badly encoded
            if (email == null || password == null)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            // Retrieve User entity from database with corresponding email, if it exists
            var user = await dbc.Users.FirstOrDefaultAsync(u => u.Email == email);

            if (user != null)
            {
                if (Crypto.VerifyHashedPassword(user.Password, password))
                {
                    // Generates a new Api Key from valid email and password
                    await dbc.SaveChangesAsync();

                    // Returns HTTP 200 with API Key and User ID if they are authorized
                    return Ok(new { user.ID, user.ApiKey });
                }
                // Returns HTTP 403 if the email or password is incorrect 
                return StatusCode(HttpStatusCode.Forbidden);
            }
            // Returns HTTP 400 when email is not found in database
            return StatusCode(HttpStatusCode.BadRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbc.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
