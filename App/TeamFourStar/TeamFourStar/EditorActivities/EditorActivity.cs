using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TeamFourStar {
    [Activity(Label = "EditorActivity")]
    public class EditorActivity : Activity {
        private ManagerContext m;
        private bool isEditing;

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.EditorLayout);

            Button cancelButton = FindViewById<Button>(Resource.Id.CancelButton);
            Button saveButton = FindViewById<Button>(Resource.Id.SaveButton);
            Button RemoveButton = FindViewById<Button>(Resource.Id.RemoveButton);

            isEditing = Intent.GetBooleanExtra("isEditing", false);

            if (!isEditing) {
                saveButton.Text = "Add";
                RemoveButton.Clickable = false;
                RemoveButton.Visibility = ViewStates.Gone;
            } else {
                RemoveButton.Click += delegate { Remove(); };
            }
            cancelButton.Click += delegate { GoBack(); };
            saveButton.Click += delegate { Save(); };

            m = enums.GetManagerContextFromString(Intent.GetStringExtra("Type"));

            switch (m) {
                case ManagerContext.Categories:
                    CategoryFrag CF = new CategoryFrag();
                    FragmentManager.BeginTransaction().Add(Resource.Id.FragContainer , CF).Commit();
                    break;
                case ManagerContext.Manufacturers:
                    ManufacturerFrag MF = new ManufacturerFrag();
                    FragmentManager.BeginTransaction().Add(Resource.Id.FragContainer , MF).Commit();
                    break;
                case ManagerContext.Products:
                    ProductFrag PF = new ProductFrag();
                    FragmentManager.BeginTransaction().Add(Resource.Id.FragContainer , PF).Commit();
                    break;
                default:
                    throw new InvalidOperationException();
            }

            if (isEditing == true) {
                //Grab ID by intent
                Populate();
            }

  
        }

        protected void Populate() {
            //Grab API values instead of blank strings

            /* switch (m) {
                case ManagerContext.Categories:
                    string[] CS = {"1","S"};
                    CategoryFrag CF = (CategoryFrag)FragmentManager.FindFragmentById(Resource.Id.FragContainer);
                    CF.Populate(CS);
                    break;
                case ManagerContext.Manufacturers:
                    string[] MS = { "33" , "M" };
                    ManufacturerFrag MF = (ManufacturerFrag)FragmentManager.FindFragmentById(Resource.Id.FragContainer);
                    MF.Populate(MS);
                    break;
                case ManagerContext.Products:
                    string[] PS = { "72" , "S", "40", "599.99" };
                    ProductFrag PF = (ProductFrag)FragmentManager.FindFragmentById(Resource.Id.FragContainer);
                    PF.Populate(PS);
                    break;
                default:
                    throw new InvalidOperationException();
            } */
        }

        protected void Save() {
            //Inputted values, see corresponding Fragments for details
            string[] results = GrabValues();
            
            //API stuff
            if(isEditing) {
                //API Save
            } else {
                //API add
            }
            Toast.MakeText(this , "Save not implimented yet, cancelling" , ToastLength.Long).Show();
            GoBack();
        }

        private string[] GrabValues() {
            switch (m) {
                case ManagerContext.Categories:
                    CategoryFrag CF = (CategoryFrag)FragmentManager.FindFragmentById(Resource.Id.FragContainer);
                    return CF.GetResults();
                case ManagerContext.Manufacturers:
                    ManufacturerFrag MF = (ManufacturerFrag)FragmentManager.FindFragmentById(Resource.Id.FragContainer);
                    return MF.GetResults();
                case ManagerContext.Products:
                    ProductFrag PF = (ProductFrag)FragmentManager.FindFragmentById(Resource.Id.FragContainer);
                    return PF.GetResults();
                default:
                    throw new InvalidOperationException();
            }
        }

        protected void Remove() {
            //APIRemove
            Toast.MakeText(this , "Remove not implimented yet, cancelling" , ToastLength.Long).Show();
            GoBack();
        }

        protected void GoBack() {
            var goBackIntent = new Intent(this , typeof(ManagerActivity));
            //Add API kay to intent
            goBackIntent.PutExtra("Selected" , enums.GetIntFromManagerContext(m));
            StartActivity(goBackIntent);
        }

    }
}