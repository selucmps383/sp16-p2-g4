﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace TeamFourStar.Models
{
	public class User
	{
		public int ID { get; set; }

		[DisplayName("Email Address")]
		public string Email { get; set; }

		public string Password { get; set; }

		public string ApiKey { get; set; }

	}
}