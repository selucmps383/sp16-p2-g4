namespace teamFourProjectTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sales", "UserID", "dbo.Users");
            DropIndex("dbo.Sales", new[] { "UserID" });
            AddColumn("dbo.Sales", "EmailAddress", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Sales", "UserID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sales", "UserID", c => c.Int(nullable: false));
            DropColumn("dbo.Sales", "EmailAddress");
            CreateIndex("dbo.Sales", "UserID");
            AddForeignKey("dbo.Sales", "UserID", "dbo.Users", "ID", cascadeDelete: true);
        }
    }
}
