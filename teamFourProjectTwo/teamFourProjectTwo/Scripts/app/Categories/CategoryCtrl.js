﻿(function () {
    'use strict';

    angular
       .module('store')
       .controller('CategoryCtrl', ['$scope','$http', 'filterFilter', 'CategoryFactory',
           function ($scope, $http, filterFilter, CategoryFactory) {
               var self = this;
              
               $scope.categories = [];
               $scope.selection = [];
               $scope.superSelection = CategoryFactory.selectedCategories;

               $http
                   .get('http://localhost:59367/api/categories')
                   .then(function (response) {
                       $scope.categories = response.data;
                   });
              
               $scope.selectedCategories = function selectedCategories() {
                   return filterFilter(self.categories, { isSelected: true });
               };

               $scope.updateSelectedCategories = function (categories) {
                   CategoryFactory.updateSelectedCategories(categories);
               };

               $scope.$watch('categories | filter:{ isSelected:true }', function (nv) {
                   $scope.selection = nv.map(function (category) {
                       return category;
                   });
                   CategoryFactory.updateSelectedCategories($scope.selection);
               }, true);

               $scope.$watch(function () { return CategoryFactory.selectedCategories }, function (newVal, oldVal) {
                   if (typeof newVal !== 'undefined') {
                       $scope.superSelection = CategoryFactory.selectedCategories;
                   }
               });
       }]);
})();