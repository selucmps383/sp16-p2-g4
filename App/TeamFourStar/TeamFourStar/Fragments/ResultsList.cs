using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TeamFourStar {
    public class ResultsList : ListFragment {
        private ManagerContext m;


        public ResultsList() { }

        //= new ArrayAdapter<string>(this ,, myStringArray);
        public override void OnActivityCreated(Bundle savedInstanceState) {
            base.OnActivityCreated(savedInstanceState);

            ListAdapter = new StoreAPIListAdapter(Activity , Resources.GetStringArray(Resource.Array.DummyValues) , m);
        
            //ListView.SetOnClickListener(this);
        }

       public override View OnCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment
            //return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            return inflater.Inflate(Resource.Layout.ResultsListFrag , container , false);
        }



        public override void OnListItemClick(ListView l , View v , int position , long id) {
            //Grab stuff from API based on Position ID which should correspond with DB ID
            var EditItemEditorIntent = new Intent(Activity , typeof(EditorActivity));

            EditItemEditorIntent.PutExtra("isEditing" , true);
            EditItemEditorIntent.PutExtra("Type" , enums.GetStringFromManagerContext(m));
            StartActivity(EditItemEditorIntent);
        }
    }
}