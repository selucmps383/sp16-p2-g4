﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using teamFourProjectTwo.Models;

namespace teamFourProjectTwo.ViewModels
{
    [NotMapped]
    public class UserViewModel : User
    {
        [Required]
        [Compare("Password")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }

        public User ToUser()
        {
            return new User()
            {
                Email = Email,
                Password = Password,
                ApiKey = ApiKey
            };
        }
    }
}