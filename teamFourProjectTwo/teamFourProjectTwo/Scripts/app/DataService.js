﻿(function () {
    'use strict';

    angular
		.module('store')
		.factory('DataService', DataService);

    function DataService() {
        var dataService = {};

        dataService.storeView = 'inventory';

        dataService.productsInCart = [];

        dataService.addToCart = function (product, quantity) {
            var element;

            if (dataService.productsInCart.length === 0) {
                element = -1;
            } else {
                angular.forEach(dataService.productsInCart, function (p) {
                    if (p.ID === product.ID) {
                        element = dataService.productsInCart.indexOf(p);
                    } else {
                        element = -1;
                    }
                });
            }

            if (element < 0) {
                product.Quantity = parseInt(quantity);
                dataService.productsInCart.push(product);
            } else {
                var existingProduct = dataService.productsInCart[element];
                existingProduct.Quantity += parseInt(quantity);
            };
        };

        dataService.removeFromCart = function (product) {
            var element = dataService.productsInCart.indexOf(product);
            var existingProduct = dataService.productsInCart[element];
            dataService.productsInCart.pop(product);
        };
        return dataService;


    }
})();        