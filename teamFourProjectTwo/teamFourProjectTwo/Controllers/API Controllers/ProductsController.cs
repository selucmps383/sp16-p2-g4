﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Description;
using teamFourProjectTwo.Models;

namespace teamFourProjectTwo.Controllers.APIControllers
{
    public class ProductsController : ApiController
    {
        private teamFourProjectTwoContext dbc = new teamFourProjectTwoContext();
        
        // GET: api/Products
        public IQueryable<Product> GetProductsByDB()
        {
            return dbc.Products;
        }

        // GET: api/Products/5
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> GetProductID(int id)
        {
            Product item = await dbc.Products.FindAsync(id);
            if (item == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return Ok(item);
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> PostItem(Product item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var now = DateTime.UtcNow;
            item.CreatedDate = now;
            item.LastModifiedDate = now;
            dbc.Products.Add(item);
            await dbc.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = item.ID }, item);
        }

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutItem(int id, Product item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.ID)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            dbc.Entry(item).State = EntityState.Modified;

            try
            {
                await dbc.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if(!ItemExists(id))
                {
                    return StatusCode(HttpStatusCode.NotFound);
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> DeleteItem(int id)
        {
            Product item = await dbc.Products.FindAsync(id);
            if (item != null)
            {
                dbc.Products.Remove(item);
                await dbc.SaveChangesAsync();
            }
            else
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return Ok(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbc.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(int id)
        {
            return dbc.Products.Count(p => p.ID == id) > 0;
        }
    }
}
