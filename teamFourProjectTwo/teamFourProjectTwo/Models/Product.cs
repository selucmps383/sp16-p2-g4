﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace teamFourProjectTwo.Models
{
    public class Product
    {
        [Key]
        public int ID { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(128)]
        [Required]
        public string Name { get; set; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate { get; set; }

        [DisplayName("Last Modified Date")]
        [DataType(DataType.DateTime)]
        public DateTime LastModifiedDate { get; set; }

        [DataType(DataType.Currency)]
        [Required]
        [Range(typeof(decimal), "0", "79228162514264337593543950335")]
        public decimal Price { get; set; }

        [DisplayName("Inventory Count")]
        [Required]
        [Range(0, int.MaxValue)]
        public int InventoryCount { get; set; }

        public string ImageUrl { get; set; }

        public virtual Category Category { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }
    
        public Product()
        {
            this.CreatedDate = DateTime.UtcNow;
            this.LastModifiedDate = DateTime.UtcNow;
        }   
    }

}