﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using teamFourProjectTwo.Services;

namespace teamFourProjectTwo.Models
{
    public class teamFourProjectTwoDBInitializer : DropCreateDatabaseAlways<teamFourProjectTwoContext>
    {
        protected override void Seed(teamFourProjectTwoContext context)
        {
            User admin = new User() {
                Email = "sa@383.com",
                Password = Crypto.HashPassword("password"),
                ApiKey = KeyGenerator.GenerateApiKey()
            };
            context.Users.Add(admin);

            var cat1 = new Category() { Name = "Crossover" };
            var cat2 = new Category() { Name = "Electric" };
            var cat3 = new Category() { Name = "Sedan" };
            var cat4 = new Category() { Name = "Sport" };
            var cat5 = new Category() { Name = "SUV" };
            var cat6 = new Category() { Name = "Truck" };


            List<Category> categories = new List<Category>
            {
                cat1,
                cat2,
                cat3,
                cat4,
                cat5,
                cat6,

            };
            foreach (var category in categories)
            {
                context.Categories.Add(category);
            }

            var man1 = new Manufacturer() { Name = "BMW" };
            var man2 = new Manufacturer() { Name = "Chevrolet" };
            var man3 = new Manufacturer() { Name = "Ford" };
            var man4 = new Manufacturer() { Name = "Jeep" };
            var man5 = new Manufacturer() { Name = "Mazda" };
            var man6 = new Manufacturer() { Name = "Nissan" };
            var man7 = new Manufacturer() { Name = "Smart" };
            var man8 = new Manufacturer() { Name = "Tesla" };
            var man9 = new Manufacturer() { Name = "Toyota" };

            List<Manufacturer> manufacturers = new List<Manufacturer>
            {
                man1,
                man2,
                man3,
                man4,
                man5,
                man6,
                man7,
                man8,
                man9,
            };
            foreach (var manufacturer in manufacturers)
            {
                context.Manufacturers.Add(manufacturer);
            }


            List<Product> products = new List<Product> {
                new Product()
                {
                    Name = "i3",
                    Category = cat2,
                    Manufacturer = man1,
                    InventoryCount = 5,
                    Price = 2.57M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/bmw/bmw_i3.jpg"
                },
                new Product()
                {
                    Name = "i8",
                    Category = cat4,
                    Manufacturer = man1,
                    InventoryCount = 3,
                    Price = 5.99M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/bmw/bmw_i8.jpg"
                },
                new Product()
                {
                    Name = "z4",
                    Category = cat4,
                    Manufacturer = man1,
                    InventoryCount = 4,
                    Price = 4.89M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/bmw/bmw_z4.jpg"
                },
                new Product()
                {
                    Name = "3 Series",
                    Category = cat4,
                    Manufacturer = man1,
                    InventoryCount = 6,
                    Price = 3.57M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/bmw/bmw_3series.jpg"
                },
                new Product()
                {
                    Name = "Camaro",
                    Category = cat4,
                    Manufacturer = man2,
                    InventoryCount = 15,
                    Price = 3.90M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/chevrolet/chev_camaro.jpg"
                },
                new Product()
                {
                    Name = "Corvette",
                    Category = cat4,
                    Manufacturer = man2,
                    InventoryCount = 10,
                    Price = 4.99M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/chevrolet/chev_corvette.jpg"
                },
                new Product()
                {
                    Name = "Silverado",
                    Category = cat6,
                    Manufacturer = man2,
                    InventoryCount = 100,
                    Price = 1.99M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/chevrolet/chev_silverado.jpg"
                },
                new Product()
                {
                    Name = "Spark",
                    Category = cat2,
                    Manufacturer = man2,
                    InventoryCount = 20,
                    Price = 1.10M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/chevrolet/chev_spark_ev.jpg"
                },
                new Product()
                {
                    Name = "Suburban",
                    Category = cat5,
                    Manufacturer = man2,
                    InventoryCount = 25,
                    Price = 2.00M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/chevrolet/chev_suburban.jpg"
                },
                new Product()
                {
                    Name = "Grand Cherokee",
                    Category = cat5,
                    Manufacturer = man4,
                    InventoryCount = 27,
                    Price = 2.15M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/jeep/grand_cherokee_2014.png"
                },
                new Product()
                {
                    Name = "Wrangler",
                    Category = cat4,
                    Manufacturer = man4,
                    InventoryCount = 14,
                    Price = 1.50M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/jeep/wrangler_unlimited_2014.png"
                },
                new Product()
                {
                    Name = "RX-8",
                    Category = cat4,
                    Manufacturer = man5,
                    InventoryCount = 16,
                    Price = 2.69M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/mazda/mazda_rx8.jpg"
                },
                new Product()
                {
                    Name = "6",
                    Category = cat3,
                    Manufacturer = man5,
                    InventoryCount = 18,
                    Price = 1.46M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/mazda/mazda6.png"
                },
                new Product()
                {
                    Name = "350z",
                    Category = cat4,
                    Manufacturer = man6,
                    InventoryCount = 10,
                    Price = 3.50M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/nissan/nissan_350z.jpg"
                },
                new Product()
                {
                    Name = "GTR",
                    Category = cat4,
                    Manufacturer = man6,
                    InventoryCount = 3,
                    Price = 5.99M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/nissan/nissan_gtr.jpg"
                },
                new Product()
                {
                    Name = "Leaf",
                    Category = cat2,
                    Manufacturer = man6,
                    InventoryCount = 12,
                    Price = 1.74M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/nissan/nissan_leaf.jpg"
                },
                new Product()
                {
                    Name = "Maxima",
                    Category = cat3,
                    Manufacturer = man6,
                    InventoryCount = 30,
                    Price = 1.75M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/nissan/nissan_maxima.jpg"
                },
                new Product()
                {
                    Name = "Murano",
                    Category = cat1,
                    Manufacturer = man6,
                    InventoryCount = 20,
                    Price = 1.60M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/nissan/nissan_murano.jpg"
                },
                new Product()
                {
                    Name = "Car",
                    Category = cat2,
                    Manufacturer = man7,
                    InventoryCount = 8,
                    Price = 1.30M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/smart/smartCar.jpg"
                },
                new Product()
                {
                    Name = "Model-S1",
                    Category = cat2,
                    Manufacturer = man8,
                    InventoryCount = 9,
                    Price = 3.89M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/tesla/Tesla-Model-S1.jpg"
                },
                new Product()
                {
                    Name = "4Runner",
                    Category = cat5,
                    Manufacturer = man9,
                    InventoryCount = 15,
                    Price = 1.57M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/toyota/toyota_4runner.jpg"
                },
                new Product()
                {
                    Name = "Titan",
                    Category = cat6,
                    Manufacturer = man9,
                    InventoryCount = 20,
                    Price = 1.59M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/toyota/toyota_titan.jpg"
                },
                new Product()
                {
                    Name = "Tundra",
                    Category = cat6,
                    Manufacturer = man9,
                    InventoryCount = 30,
                    Price = 1.89M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/toyota/toyota_tundra.jpg"
                },
                new Product()
                {
                    Name = "Explorer",
                    Category = cat5,
                    Manufacturer = man3,
                    InventoryCount = 30,
                    Price = 5.43M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/ford/ford_explorer.png"
                },
                 new Product()
                {
                    Name = "F-150",
                    Category = cat6,
                    Manufacturer = man3,
                    InventoryCount = 30,
                    Price = 9.33M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/ford/ford_f150.png"
                },
                new Product()
                {
                    Name = "Fusion",
                    Category = cat3,
                    Manufacturer = man3,
                    InventoryCount = 32,
                    Price = 5.32M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/ford/ford_fusion.png"
                },
                new Product()
                {
                    Name = "Mustang",
                    Category = cat4,
                    Manufacturer = man3,
                    InventoryCount = 30,
                    Price = 4.23M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/ford/ford_mustang.png"
                },
                new Product()
                {
                    Name = "Taurus",
                    Category = cat3,
                    Manufacturer = man3,
                    InventoryCount = 50,
                    Price = 8.15M,
                    CreatedDate = DateTime.UtcNow,
                    LastModifiedDate = DateTime.UtcNow,
                    ImageUrl = "../Content/assets/images/products/ford/ford_taurus.png"
                }
            };

            foreach(var product in products)
            {
                context.Products.Add(product);
            }
            
            base.Seed(context);
        }
    }
}