using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TeamFourStar {
    [Activity(Label = "StoreManagement")]
    public class ManagerActivity : Activity {

       public ManagerContext m { get; private set; }
        //protected static ResultsList R;

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.ManagerPage);

            //Example of grabbing data from an intent
            //string message = Intent.GetStringExtra("Message") ?? "Whoops!";
            //Toast.MakeText(this , message ,ToastLength.Long).Show();

            Button addButton = FindViewById<Button>(Resource.Id.AddButton);
            addButton.Click += delegate { GoToEditorAdd(); };

            //Set up Category Spinner
            Spinner CategorySelector = CatSpinnerSetup();

            //Populate Listview
            //PopulateList(true);
        }

        protected Spinner CatSpinnerSetup() {
            Spinner Cat = FindViewById<Spinner>(Resource.Id.catSpinner);

            Cat.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                this , Resource.Array.ContextCategories , Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            Cat.Adapter = adapter;
           
            if(Intent.GetIntExtra("Selected" , -1) != -1) {
                int i = Intent.GetIntExtra("Selected" , -1);
                Cat.SetSelection(i); 
                
            }
            return Cat;
        }


        private void spinner_ItemSelected(object sender , AdapterView.ItemSelectedEventArgs e) {
            Spinner spinner = (Spinner)sender;
            SwapManagerContext((string)spinner.GetItemAtPosition(e.Position));
            //PopulateList(false);
        }

        private void SwapManagerContext(string s) {
            m = enums.GetManagerContextFromString(s);        
        }

        /*private void PopulateList(bool isNew) {
            if(!isNew) {
                FragmentManager.BeginTransaction().Remove(R).Commit();
            }
            R = new ResultsList();
            //FragmentManager.BeginTransaction().Add(Resource.Id.).Commit(); 
        }*/

        //Add button opens this
        private void GoToEditorAdd() {
            var AddItemEditorIntent = new Intent(this , typeof(EditorActivity));

            AddItemEditorIntent.PutExtra("isEditing" , false);
            AddItemEditorIntent.PutExtra("Type" , enums.GetStringFromManagerContext(m));
            StartActivity(AddItemEditorIntent);
        }
    }
}