﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using teamFourProjectTwo.Models;

namespace teamFourProjectTwo.Controllers.APIControllers
{
    public class ManufacturersController : ApiController
    {
        private teamFourProjectTwoContext dbc = new teamFourProjectTwoContext();

        // GET: api/Manufacturers
        public IQueryable<Manufacturer> GetManufacturersByDB()
        {
            return dbc.Manufacturers;
        }

        // GET: api/Manufacturers/5
        [ResponseType(typeof(Manufacturer))]
        public async Task<IHttpActionResult> GetManufacturerId(int id)
        {
            Manufacturer company = await dbc.Manufacturers.FindAsync(id);
            if (company == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return Ok(company);
        }
    
        // POST: api/Manufacturers
        [ResponseType(typeof(Manufacturer))]
        public async Task<IHttpActionResult> PostManufacturer(Manufacturer company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            dbc.Manufacturers.Add(company);
            await dbc.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = company.ID }, company);
        }

        // PUT: api/Manufacturers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutManufacturer(int id, Manufacturer company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != company.ID)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            dbc.Entry(company).State = EntityState.Modified;
            try
            {
                await dbc.SaveChangesAsync();
            }
            
            catch (DbUpdateConcurrencyException)
            {
                if (!ManufacturerExists(id))
                {
                    return StatusCode(HttpStatusCode.NotFound);
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Manufacturers/5
        [ResponseType(typeof(Manufacturer))]
        public async Task<IHttpActionResult> DeleteManufacturer(int id)
        {
            Manufacturer company = await dbc.Manufacturers.FindAsync(id);
            if (company != null)
            {
                dbc.Manufacturers.Remove(company);
                await dbc.SaveChangesAsync();
            }
            else
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return Ok(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbc.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ManufacturerExists(int id)
        {
            return dbc.Manufacturers.Count(p => p.ID == id) > 0;
        }
    }
}
