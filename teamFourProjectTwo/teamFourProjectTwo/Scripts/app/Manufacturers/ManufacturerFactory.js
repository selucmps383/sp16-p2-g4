﻿(function () {
    'use strict';

    angular
		.module('store')
		.factory('ManufacturerFactory', ManufacturerFactory);

    ManufacturerFactory.$inject = ['$http'];

    function ManufacturerFactory() {
        var manufacturerFactory = {};

        manufacturerFactory.selectedManufacturers = [];

        manufacturerFactory.updateSelectedManufacturers = function (manufacturers) {
            manufacturerFactory.selectedManufacturers = manufacturers;
        };
        return manufacturerFactory;
    }
})();