﻿(function () {
    'use strict';

    angular
		.module('store')
		.config(['$routeProvider',
            function ($routeProvider) {
                $routeProvider.
                    when('/shop', {
                        templateUrl: 'scripts/app/templates/pages/inventory/index.html'
                    }).
                    when('/cart', {
                        templateUrl: 'scripts/app/templates/pages/checkout/cart.html',
                        controllerAs: 'cart',
                        controller: 'CartCtrl'
                    }).
                    when('/review', {
                        templateUrl: 'scripts/app/templates/pages/checkout/review.html',
                        controllerAs: 'cart',
                        controller: 'CartCtrl'
                    }).
                    when('/', {
                        templateUrl: 'scripts/app/templates/pages/inventory/index.html'
                    }).
                    otherwise({ redirectTo: '/' });
            }]);
})();