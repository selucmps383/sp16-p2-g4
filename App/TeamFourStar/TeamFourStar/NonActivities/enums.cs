using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TeamFourStar {
        public enum ManagerContext {
            Products,
            Manufacturers,
            Categories
    }
    public static class enums {
        public static string GetStringFromManagerContext(ManagerContext m) {
            switch(m) {
                case ManagerContext.Products:
                    return "Products";
                case ManagerContext.Manufacturers:
                    return "Manufactuers";
                case ManagerContext.Categories:
                    return "Categories";
                default:
                    throw new InvalidCastException();
            } 
        }

        public static ManagerContext GetManagerContextFromString(string s) {
            switch(s) {
                case "Categories":
                    return ManagerContext.Categories;
                case "Manufactuers":
                    return ManagerContext.Manufacturers;
                case "Products":
                    return ManagerContext.Products;
                default:
                    throw new InvalidCastException();
            }
        }

        public static int GetIntFromManagerContext(ManagerContext m) {
            switch (m) {
                case ManagerContext.Categories:
                    return 2;
                case ManagerContext.Manufacturers:
                    return 1;
                case ManagerContext.Products:
                    return 0;
                default:
                    throw new InvalidCastException();
            }
        }

    }
}