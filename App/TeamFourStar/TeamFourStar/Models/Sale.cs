﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace TeamFourStar.Models
{
	public class Sale
	{
		public int ID { get; set; }

		[DisplayName("Purchase Date")]
		public DateTime Date { get; set; }

		public List<Product> Products { get; set; }

		[DisplayName("Total Amount")]
		public decimal TotalAmount { get; set; }

		[DisplayName("Email Address")]
		public int UserID { get; set; }

		public virtual User User { get; set; }
	}
}