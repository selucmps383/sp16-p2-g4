﻿(function () {
    'use strict';

    angular
       .module('store')
       .controller('CartCtrl', ['$scope', '$http', 'DataService',
           function ($scope, $http, DataService) {
               var self = this;

               self.email;
               
               self.invoice = {
                   products: DataService.productsInCart
               };   
               
               self.removeFromCart = function (product) {
                   DataService.removeFromCart(product);
               };

               self.isInStock = function (product) {
                   if (product.InventoryCount > 0) {
                       return "In Stock";
                   } else {
                       return "Out of Stock";
                   }
               }

               self.total = function() {
                   var total = 0;
                   angular.forEach(self.invoice.products, function(product) {
                       total += product.Quantity * product.Price;
                   })
                   return parseFloat(total);
               };

               self.totalCount = function () {
                   var total = 0;
                   angular.forEach(self.invoice.products, function (product) {
                       total += product.Quantity;
                   })
                   return parseFloat(total);
               };

               self.placeOrder = function () {
                   var data = $.param({
                       json: JSON.stringify({
                           headers: {
                               'Content-Type': 'application/json'
                           },
                           Products: self.invoice.products,
                           TotalAmount: self.total,
                           EmailAddress: self.email
                       })
                   });
                   
                   $http.post("/api/sales", data).success(function (data, status) {
                       $scope.hello = status;
                   })
               }
           }]);
})();