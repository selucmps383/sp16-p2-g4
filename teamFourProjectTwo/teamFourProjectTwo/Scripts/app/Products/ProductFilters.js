﻿(function () {
    'use strict';

    angular
        .module('store')
        .filter('byCategory',
            function byCategory() {
                return function (products, categories) {
                    var out = [];
                    var categoryIds = [];

                    angular.forEach(categories, function (category) {
                        categoryIds.push(category.ID);
                    });

                    angular.forEach(products, function (product) {
                        if (categoryIds.length < 1) {
                            out.push(product);
                        } else if (categoryIds.indexOf(product.Category.ID) > -1) {
                            out.push(product);
                        }
                    });
                    return out;
                }
        }).filter('byManufacturer',
            function byManufacturer() {
                return function (products, manufacturers) {
                    var out = [];
                    /*
                    angular.forEach(products, function (product) {
                        if (manufacturers.length < 1) {
                            out.push(product);
                        } else if (manufacturers.indexOf(product.Category.ID) > -1) {
                            out.push(product);
                        }
                    });
                    return out;*/
                    var manufacturerIds = [];

                    angular.forEach(manufacturers, function (manufacturer) {
                        manufacturerIds.push(manufacturer.ID);
                    });

                    angular.forEach(products, function (product) {
                        if (manufacturerIds.length < 1) {
                            out.push(product);
                        } else if (manufacturerIds.indexOf(product.Manufacturer.ID) > -1) {
                            out.push(product);
                        }
                    });
                    return out;
                }
            });
})();