﻿// Wrap everything in an IIFE
(function ($, viewport) {

    var main = $('#main-container');

    if (viewport.is('xs')) {
        if (main.hasClass('container')) {
            main.removeClass('container');
        }
    }
    if (viewport.is('sm')) {
        if (main.hasClass('container')) {
            main.removeClass('container');
        }
    }
    if (viewport.is('md')) {
        if (!main.hasClass('container')) {
            main.addClass('container');
        }
    }
    if (viewport.is('lg')) {
        if (!main.hasClass('container')) {
            main.addClass('container');
        }
    }

    // Execute only after document has fully loaded
    $(document).ready(function () {
        var main = $('#main-container');

        if (viewport.is('xs')) {
            if (main.hasClass('container')) {
                main.removeClass('container');
            }
        }
        if (viewport.is('sm')) {
            if (main.hasClass('container')) {
                main.removeClass('container');
            }
        }
        if (viewport.is('md')) {
            if (!main.hasClass('container')) {
                main.addClass('container');
            }
        }
        if (viewport.is('lg')) {
            if (!main.hasClass('container')) {
                main.addClass('container');
            }
        }
    });

    // Execute code each time window size changes
    $(window).resize(
        viewport.changed(function () {
            var main = $('#main-container');

            if (viewport.is('xs')) {
                if (main.hasClass('container')) {
                    main.removeClass('container');
                }
            }
            if (viewport.is('sm')) {
                if (main.hasClass('container')) {
                    main.removeClass('container');
                }
            }
            if (viewport.is('md')) {
                if (!main.hasClass('container')) {
                    main.addClass('container');
                }
            }
            if (viewport.is('lg')) {
                if (!main.hasClass('container')) {
                    main.addClass('container');
                }
            }
        })
    );

})(jQuery, ResponsiveBootstrapToolkit);