﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace teamFourProjectTwo.Models
{
    public class Category
    {
        [Key]
        public int ID { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(128)]
        [DisplayName("Category Name")]
        [Required]
        public string Name { get; set; }

        public List<Product> Products { get; set; }
    }
}