﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using teamFourProjectTwo.Models;

namespace teamFourProjectTwo.Controllers.APIControllers
{
    public class CategoriesController : ApiController
    {
       private teamFourProjectTwoContext dbc = new teamFourProjectTwoContext();

            // GET: api/Categories
            public IQueryable<Category> GetCategoriesByDB()
            {
                return dbc.Categories;
            }

            // GET: api/Categories/5
            [ResponseType(typeof(Category))]
            public async Task<IHttpActionResult> GetCategoryId(int id)
            {
                Category genre = await dbc.Categories.FindAsync(id);
                if (genre == null)
                {
                    return StatusCode(HttpStatusCode.NotFound);
                }

                return Ok(genre);
            }

            // POST: api/Categories
            [ResponseType(typeof(Category))]
            public async Task<IHttpActionResult> PostCategory(Category genre)
            {
                if(!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                dbc.Categories.Add(genre);
                await dbc.SaveChangesAsync();

                return CreatedAtRoute("DefaultApi", new { id = genre.ID }, genre);
            }

            // PUT: api/Categories/5
            [ResponseType(typeof(void))]
            public async Task<IHttpActionResult> PutCategory(int id, Category genre)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (id != genre.ID)
                {
                    return StatusCode(HttpStatusCode.BadRequest);
                }

                dbc.Entry(genre).State = EntityState.Modified;

                try
                {
                    await dbc.SaveChangesAsync();
                }

                catch (DbUpdateConcurrencyException)
                {
                    if(!CategoryExists(id))
                    {
                        return StatusCode(HttpStatusCode.NotFound);
                    }
                    else
                    {
                        throw;
                    }
                }

                return StatusCode(HttpStatusCode.NoContent);
            }

            // DELETE: api/Categories/5
            [ResponseType(typeof(Category))]
            public async Task<IHttpActionResult> DeleteCategory(int id)
            {
                Category genre = await dbc.Categories.FindAsync(id);
                if (genre != null)
                {
                    dbc.Categories.Remove(genre);
                    await dbc.SaveChangesAsync();
                }
                else
                {
                    return StatusCode(HttpStatusCode.NotFound);
                }
                return Ok(genre);
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    dbc.Dispose();
                }
                base.Dispose(disposing);
            }

            private bool CategoryExists(int id)
            {
                return dbc.Categories.Count(p => p.ID == id) > 0;
            }
        }
    }
