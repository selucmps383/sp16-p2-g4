using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TeamFourStar {
    public class ProductFrag : EditorFrag {
        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            return inflater.Inflate(Resource.Layout.ProdFrag , container , false);
        }

        public override void Populate(string[] ProductData) {
            EditText ID = View.FindViewById<EditText>(Resource.Id.IDEdit);
            EditText Name = View.FindViewById<EditText>(Resource.Id.NameEdit);
            EditText Amount = View.FindViewById<EditText>(Resource.Id.InventoryAmtEdit);
            EditText Price = View.FindViewById<EditText>(Resource.Id.EditPrice);

            ID.Text = ProductData[0];
            Name.Text = ProductData[1];
            Amount.Text = ProductData[2];
            Price.Text = ProductData[3];
        }

        public override string[] GetResults() {
            string ID = View.FindViewById<EditText>(Resource.Id.IDEdit).Text;
            string Name = View.FindViewById<EditText>(Resource.Id.NameEdit).Text;
            string Amount = View.FindViewById<EditText>(Resource.Id.InventoryAmtEdit).Text;
            string Price = View.FindViewById<EditText>(Resource.Id.EditPrice).Text;

            string[] s = { ID , Name, Amount , Price };
            return s;
        }
    }
}