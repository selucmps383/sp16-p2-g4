﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using teamFourProjectTwo.Models;
using System.Web.Helpers;
using System.Web.Security;
using teamFourProjectTwo.ViewModels;
using teamFourProjectTwo.Services;

namespace teamFourProjectTwo.Controllers
{
    public class UsersController : Controller
    {
        private teamFourProjectTwoContext db = new teamFourProjectTwoContext();

        // GET: Users
        [Authorize]
        public async Task<ActionResult> Index()
        {
            return View(await db.Users.ToListAsync());
        }

        // GET: Users/Details/5
        [Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Email,Password,ConfirmPassword")] UserViewModel userVM)
        {
            if (ModelState.IsValid)
            {
                userVM.Password = Crypto.HashPassword(userVM.Password);
                userVM.ApiKey = KeyGenerator.GenerateApiKey();
                db.Users.Add(userVM.ToUser());
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(userVM);
        }

        // GET: Users/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            user.Password = "";
            return View(user.ToViewModel());
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Email,Password,ConfirmPassword")] UserViewModel userVM)
        {
            if (ModelState.IsValid)
            {
                var user = await db.Users.FindAsync(userVM.ID);
                user.Email = userVM.Email;
                user.Password = Crypto.HashPassword(userVM.Password);

                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            userVM.Password = "";
            userVM.ConfirmPassword = "";
            return View(userVM);
        }

        // GET: Users/Delete/5
        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(string email, string password)
        {
            var loginUser = await db.Users.FirstOrDefaultAsync(u => u.Email == email);
            if (loginUser != null)
            {
                if (Crypto.VerifyHashedPassword(loginUser.Password, password))
                {
                    FormsAuthentication.SetAuthCookie(loginUser.Email, false);
                    return RedirectToAction("Index", "Users");
                }
            }
            ModelState.AddModelError("Email", "Email Address or Password was Incorrect");
            return View(new User() { Email = email });

        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}
