namespace teamFourProjectTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SaleModelUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Sale_ID", c => c.Int());
            CreateIndex("dbo.Products", "Sale_ID");
            AddForeignKey("dbo.Products", "Sale_ID", "dbo.Sales", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Sale_ID", "dbo.Sales");
            DropIndex("dbo.Products", new[] { "Sale_ID" });
            DropColumn("dbo.Products", "Sale_ID");
        }
    }
}
