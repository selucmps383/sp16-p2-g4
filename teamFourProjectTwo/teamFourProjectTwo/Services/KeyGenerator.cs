﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace teamFourProjectTwo.Services
{
    public class KeyGenerator
    {
        // Method Provided from Assignment Technical Requirements (http://pastebin.com/PstjKqpk)
        public static string GenerateApiKey()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }
    }
}