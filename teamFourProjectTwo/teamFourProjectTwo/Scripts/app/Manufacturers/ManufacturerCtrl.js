﻿(function () {
    'use strict';

    angular
        .module('store')
        .controller('ManufacturerCtrl', ['$scope','$http', 'filterFilter', 'ManufacturerFactory',
            function ($scope, $http, filterFilter, ManufacturerFactory) {
                var self = this;

                $scope.manufacturers = [];
                $scope.selection = [];
                $scope.superSelection = ManufacturerFactory.selectedManufacturers;

                $http
                    .get('http://localhost:59367/api/manufacturers')
                    .then(function (response) {
                        $scope.manufacturers = response.data;
                    });
                
                $scope.selectedManufacturers = function selectedManufacturers() {
                    return filterFilter(self.manufacturers, { isSelected: true });
                };

                $scope.updateSelectedManufacturers = function (manufacturers) {
                    ManufacturerFactory.updateSelectedManufacturers(manufacturers);
                };

                $scope.$watch('manufacturers | filter:{ isSelected:true }', function (nv) {
                    $scope.selection = nv.map(function (manufacturer) {
                        return manufacturer;
                    });
                    ManufacturerFactory.updateSelectedManufacturers($scope.selection);
                }, true);

                $scope.$watch(function () { return ManufacturerFactory.selectedManufacturers }, function (newVal, oldVal) {
                    if (typeof newVal !== 'undefined') {
                        $scope.superSelection = ManufacturerFactory.selectedManufacturers;
                    }
                });
        }]);
})();           
