﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

	namespace TeamFourStar.Models
	{
		public class Category
		{
			public int ID { get; set; }
			[DisplayName("Category Name")]
			public string Name { get; set; }
			public List<Product> Products { get; set; }
		}
	}