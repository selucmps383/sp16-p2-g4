﻿(function () {
    'use strict';

    angular
		.module('store')
		.factory('CategoryFactory', CategoryFactory);

    CategoryFactory.$inject = ['$http'];

    function CategoryFactory() {
        var categoryFactory = {};
        
        categoryFactory.selectedCategories = [];

        categoryFactory.updateSelectedCategories = function (categories) {
            categoryFactory.selectedCategories = categories;
        };
        return categoryFactory;
    }
})();